package de.wellnerbou.keyworder.search;

import com.google.common.collect.Lists;
import de.wellnerbou.keyworder.loader.Keyword;
import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.analysis.de.GermanAnalyzer;
import org.junit.Test;

import java.util.Collection;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class LuceneKeywordInTextSearcherTest {

    @Test
    public void testSearch() {
        Keyword keywordFrankfurt = new Keyword("Frankfurt", "Place");
        final Collection<Keyword> keywords = Lists.newArrayList(keywordFrankfurt, new Keyword("New York", "Place"));
        final String haystack = "Frankfurt am Main ist mit gut 730.000 Einwohnern die größte Stadt Hessens und die fünftgrößte Stadt Deutschlands. Die kreisfreie Stadt ist Zentrum des Ballungsraums Frankfurt-Rhein-Main mit etwa 2,2 Millionen[2] Einwohnern. In der erweiterten Stadtregion Frankfurt leben etwa 2,5 Millionen Menschen (2012),[3] in der gesamten Metropolregion Rhein-Main etwa 5,5 Millionen.";

        List<Result> results = new LuceneKeywordInTextSearcher(new GermanAnalyzer()).search(keywords, haystack);
        assertThat(results).hasSize(1);
        assertThat(results.get(0).getKeyword()).isEqualTo(keywordFrankfurt);
        assertThat(results.get(0).getRelevance()).isGreaterThan(0.4f);
    }

    @Test
    public void testSearch_fuzzy() {
        Keyword keywordFrankfurt = new Keyword("Frankfurt", "Place");
        final Collection<Keyword> keywords = Lists.newArrayList(keywordFrankfurt);
        final String haystack = "Alle Frankfurter trinken Äppler";

        List<Result> results = new LuceneKeywordInTextSearcher(new GermanAnalyzer()).search(keywords, haystack);
        assertThat(results).hasSize(1);
    }

    @Test
    public void testSearch_ambiguity() {
        Keyword keywordFrankfurtMain = new Keyword("Frankfurt", "Place");
        final Collection<Keyword> keywords = Lists.newArrayList(keywordFrankfurtMain);
        final String haystack = "Alle Frankfurter trinken Äppler";

        List<Result> results = new LuceneKeywordInTextSearcher(new GermanAnalyzer()).search(keywords, haystack);
        assertThat(results).hasSize(1);
        assertThat(results.get(0).getKeyword()).isEqualTo(keywordFrankfurtMain);
    }

    @Test
    public void testSearch_checkSearchWithSpaces() {
        Keyword keywordFrankfurt = new Keyword("Frankfurt am Main", "Place");
        final Collection<Keyword> keywords = Lists.newArrayList(keywordFrankfurt);
        final String haystack = "In Frankfurt an der Oder trinkt man kein Äppler";

        List<Result> results = new LuceneKeywordInTextSearcher(new GermanAnalyzer()).search(keywords, haystack);
        assertThat(results).hasSize(0);
    }
}
