package de.wellnerbou.keyworder.loader.csv;

import com.google.common.io.CharSource;
import de.wellnerbou.keyworder.loader.Keyword;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Collection;

import static org.junit.Assert.*;

public class CsvKeywordLoaderTest {

    @Test
    public void loadKeywords() throws IOException {
        CsvKeywordLoader csvKeywordLoader = new CsvKeywordLoader(
                new ByteArrayInputStream("Frankfurt;Place\nPaul Wellner Bou;Person\nGermany;Place\n".getBytes()),
                new CsvReadingConfig(";", 0, 1)
        );
        Collection<Keyword> keywords = csvKeywordLoader.loadKeywords();
        Assertions.assertThat(keywords).containsExactlyInAnyOrder(
                new Keyword("Frankfurt", "Place"),
                new Keyword("Germany", "Place"),
                new Keyword("Paul Wellner Bou", "Person")
        );
    }
}
