package de.wellnerbou.keyworder.loader.csv;

public class CsvReadingConfig {

    private final String separator;
    private final int keywordIndex;
    private final int categoryIndex;

    public CsvReadingConfig(String separator, int keywordIndex, int categoryIndex) {
        this.separator = separator;
        this.keywordIndex = keywordIndex;
        this.categoryIndex = categoryIndex;
    }

    public String getSeparator() {
        return separator;
    }

    public int getKeywordIndex() {
        return keywordIndex;
    }

    public int getCategoryIndex() {
        return categoryIndex;
    }
}