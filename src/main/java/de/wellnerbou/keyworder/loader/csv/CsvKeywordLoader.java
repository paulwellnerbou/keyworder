package de.wellnerbou.keyworder.loader.csv;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import de.wellnerbou.keyworder.loader.Keyword;
import de.wellnerbou.keyworder.loader.KeywordLoader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

public class CsvKeywordLoader implements KeywordLoader {

    private InputStream csvInputStream;
    private CsvReadingConfig csvReadingConfig;

    public CsvKeywordLoader(final InputStream csvInputStream, final CsvReadingConfig csvReadingConfig) {
        this.csvInputStream = csvInputStream;
        this.csvReadingConfig = csvReadingConfig;
    }

    @Override
    public Collection<Keyword> loadKeywords() {
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(csvInputStream))) {
            return bufferedReader.lines().map(this::createKeyword).collect(Collectors.toCollection(ArrayList::new));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Keyword createKeyword(String line) {
        ArrayList<String> values = Lists.newArrayList(Splitter.on(csvReadingConfig.getSeparator()).split(line));
        return new Keyword(values.get(csvReadingConfig.getKeywordIndex()), values.get(csvReadingConfig.getCategoryIndex()));
    }
}
