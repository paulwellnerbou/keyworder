package de.wellnerbou.keyworder.loader;

import java.util.Collection;

public interface KeywordLoader {
    Collection<Keyword> loadKeywords();
}
