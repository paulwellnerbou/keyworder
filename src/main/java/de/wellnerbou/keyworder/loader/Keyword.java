package de.wellnerbou.keyworder.loader;

import java.util.Objects;

public class Keyword {

    private String keyword;
    private String category;

    public Keyword(String keyword, String category) {
        this.keyword = keyword;
        this.category = category;
    }

    public String getCategory() {
        return category;
    }

    public String getKeyword() {
        return keyword;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Keyword keyword1 = (Keyword) o;
        return Objects.equals(keyword, keyword1.keyword) &&
                Objects.equals(category, keyword1.category);
    }

    @Override
    public int hashCode() {
        return Objects.hash(keyword, category);
    }

    @Override
    public String toString() {
        return "Keyword{" +
                "keyword='" + keyword + '\'' +
                ", category='" + category + '\'' +
                '}';
    }
}
