package de.wellnerbou.keyworder;

import de.wellnerbou.keyworder.loader.Keyword;
import de.wellnerbou.keyworder.loader.KeywordLoader;
import de.wellnerbou.keyworder.search.Result;
import de.wellnerbou.keyworder.search.Searcher;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Keyworder {

    private KeywordLoader keywordLoader;
    private Searcher searcher;
    private Collection<Keyword> keywords;

    public Keyworder(KeywordLoader keywordLoader, Searcher searcher) {
        this.keywordLoader = keywordLoader;
        this.searcher = searcher;
    }

    public List<Result> keywords(CharSequence text) {
        return searcher.search(getKeywords(), text);
    }

    public List<Result> keywordsSorted(CharSequence text, long limit) {
        List<Result> keywords = this.keywords(text);
        keywords.sort(Comparator.comparing(Result::getRelevance));
        return keywords.stream().limit(limit).collect(Collectors.toList());
    }

    private Collection<Keyword> getKeywords() {
        if(keywords == null || keywords.size() == 0) {
            keywords = keywordLoader.loadKeywords();
        }
        return keywords;
    }
}
