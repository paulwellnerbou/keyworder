package de.wellnerbou.keyworder.search;

import de.wellnerbou.keyworder.loader.Keyword;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.de.GermanAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.store.RAMDirectory;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class LuceneKeywordInTextSearcher implements Searcher {

    private Analyzer analyzer;

    public LuceneKeywordInTextSearcher(Analyzer analyzer) {
        this.analyzer = analyzer;
    }

    public List<Result> search(Collection<Keyword> keywords, CharSequence text) {
        RAMDirectory directory = createIndex(text);
        try (DirectoryReader directoryReader = DirectoryReader.open(directory);) {
            IndexSearcher indexSearcher = new IndexSearcher(directoryReader);
            QueryParser parser = new QueryParser("text", analyzer);
            return keywords.stream().map(keyword -> this.searchForKeyword(indexSearcher, parser, keyword)).filter(Objects::nonNull).collect(Collectors.toList());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Result searchForKeyword(IndexSearcher indexSearcher, QueryParser parser, Keyword keyword) {
        try {
            Query query = parser.parse("text:\"" + keyword.getKeyword() + "\"");
            ScoreDoc[] scoreDocs = indexSearcher.search(query, 1).scoreDocs;
            if (scoreDocs.length > 0) {
                return new Result(keyword, scoreDocs[0].score);
            } else {
                return null;
            }
        } catch (ParseException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    private RAMDirectory createIndex(CharSequence text) {
        RAMDirectory idx = new RAMDirectory();
        final IndexWriterConfig config = new IndexWriterConfig(analyzer);
        Document doc = new Document();
        doc.add(new TextField("text", text.toString(), Field.Store.NO));
        try (final IndexWriter writer = new IndexWriter(idx, config);) {
            writer.addDocument(doc);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return idx;
    }
}
