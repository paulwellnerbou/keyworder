package de.wellnerbou.keyworder.search;

import de.wellnerbou.keyworder.loader.Keyword;

public class Result {

    public Keyword getKeyword() {
        return keyword;
    }

    public Float getRelevance() {
        return relevance;
    }

    private final Keyword keyword;
    private final float relevance;

    public Result(Keyword keyword, Float relevance){
        this.keyword = keyword;
        this.relevance = relevance;
    }
}
