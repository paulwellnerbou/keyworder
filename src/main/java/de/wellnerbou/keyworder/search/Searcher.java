package de.wellnerbou.keyworder.search;

import de.wellnerbou.keyworder.loader.Keyword;

import java.util.Collection;
import java.util.List;

public interface Searcher {
    List<Result> search(Collection<Keyword> keywords, CharSequence text);
}
